package DAO;

import Entidad.Empleado;
import android.database.Cursor;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;
import androidx.room.RoomSQLiteQuery;
import androidx.room.SharedSQLiteStatement;
import androidx.room.util.CursorUtil;
import androidx.room.util.DBUtil;
import androidx.sqlite.db.SupportSQLiteStatement;
import java.lang.Class;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@SuppressWarnings({"unchecked", "deprecation"})
public final class EmpleadoDAO_Impl implements EmpleadoDAO {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter<Empleado> __insertionAdapterOfEmpleado;

  private final SharedSQLiteStatement __preparedStmtOfEliminaEmpleado;

  private final SharedSQLiteStatement __preparedStmtOfActualizarProNoControl;

  public EmpleadoDAO_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfEmpleado = new EntityInsertionAdapter<Empleado>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR ABORT INTO `Empleado` (`emp_no`,`birth_date`,`first_name`,`last_name`,`gender`,`hire_date`) VALUES (?,?,?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, Empleado value) {
        stmt.bindLong(1, value.getEmp_no());
        if (value.getBirth_date() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getBirth_date());
        }
        if (value.getFirtst_name() == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.getFirtst_name());
        }
        if (value.getLast_name() == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.getLast_name());
        }
        if (value.getGender() == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, value.getGender());
        }
        if (value.getHire_date() == null) {
          stmt.bindNull(6);
        } else {
          stmt.bindString(6, value.getHire_date());
        }
      }
    };
    this.__preparedStmtOfEliminaEmpleado = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "DELETE FROM Empleado WHERE emp_no=?";
        return _query;
      }
    };
    this.__preparedStmtOfActualizarProNoControl = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "UPDATE Empleado SET birth_date=?,first_name=?,last_name=?,gender=?,hire_date=? WHERE emp_no=?";
        return _query;
      }
    };
  }

  @Override
  public void insertarEmpleado(final Empleado em) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __insertionAdapterOfEmpleado.insert(em);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void eliminaEmpleado(final String emno) {
    __db.assertNotSuspendingTransaction();
    final SupportSQLiteStatement _stmt = __preparedStmtOfEliminaEmpleado.acquire();
    int _argIndex = 1;
    if (emno == null) {
      _stmt.bindNull(_argIndex);
    } else {
      _stmt.bindString(_argIndex, emno);
    }
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfEliminaEmpleado.release(_stmt);
    }
  }

  @Override
  public void actualizarProNoControl(final int n, final String x, final String y, final String z,
      final String e, final String s) {
    __db.assertNotSuspendingTransaction();
    final SupportSQLiteStatement _stmt = __preparedStmtOfActualizarProNoControl.acquire();
    int _argIndex = 1;
    if (x == null) {
      _stmt.bindNull(_argIndex);
    } else {
      _stmt.bindString(_argIndex, x);
    }
    _argIndex = 2;
    if (y == null) {
      _stmt.bindNull(_argIndex);
    } else {
      _stmt.bindString(_argIndex, y);
    }
    _argIndex = 3;
    if (z == null) {
      _stmt.bindNull(_argIndex);
    } else {
      _stmt.bindString(_argIndex, z);
    }
    _argIndex = 4;
    if (e == null) {
      _stmt.bindNull(_argIndex);
    } else {
      _stmt.bindString(_argIndex, e);
    }
    _argIndex = 5;
    if (s == null) {
      _stmt.bindNull(_argIndex);
    } else {
      _stmt.bindString(_argIndex, s);
    }
    _argIndex = 6;
    _stmt.bindLong(_argIndex, n);
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfActualizarProNoControl.release(_stmt);
    }
  }

  @Override
  public List<Empleado> optenerTodos() {
    final String _sql = "SELECT * FROM Empleado";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfEmpNo = CursorUtil.getColumnIndexOrThrow(_cursor, "emp_no");
      final int _cursorIndexOfBirthDate = CursorUtil.getColumnIndexOrThrow(_cursor, "birth_date");
      final int _cursorIndexOfFirtstName = CursorUtil.getColumnIndexOrThrow(_cursor, "first_name");
      final int _cursorIndexOfLastName = CursorUtil.getColumnIndexOrThrow(_cursor, "last_name");
      final int _cursorIndexOfGender = CursorUtil.getColumnIndexOrThrow(_cursor, "gender");
      final int _cursorIndexOfHireDate = CursorUtil.getColumnIndexOrThrow(_cursor, "hire_date");
      final List<Empleado> _result = new ArrayList<Empleado>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final Empleado _item;
        final int _tmpEmp_no;
        _tmpEmp_no = _cursor.getInt(_cursorIndexOfEmpNo);
        final String _tmpBirth_date;
        if (_cursor.isNull(_cursorIndexOfBirthDate)) {
          _tmpBirth_date = null;
        } else {
          _tmpBirth_date = _cursor.getString(_cursorIndexOfBirthDate);
        }
        final String _tmpFirtst_name;
        if (_cursor.isNull(_cursorIndexOfFirtstName)) {
          _tmpFirtst_name = null;
        } else {
          _tmpFirtst_name = _cursor.getString(_cursorIndexOfFirtstName);
        }
        final String _tmpLast_name;
        if (_cursor.isNull(_cursorIndexOfLastName)) {
          _tmpLast_name = null;
        } else {
          _tmpLast_name = _cursor.getString(_cursorIndexOfLastName);
        }
        final String _tmpGender;
        if (_cursor.isNull(_cursorIndexOfGender)) {
          _tmpGender = null;
        } else {
          _tmpGender = _cursor.getString(_cursorIndexOfGender);
        }
        final String _tmpHire_date;
        if (_cursor.isNull(_cursorIndexOfHireDate)) {
          _tmpHire_date = null;
        } else {
          _tmpHire_date = _cursor.getString(_cursorIndexOfHireDate);
        }
        _item = new Empleado(_tmpEmp_no,_tmpBirth_date,_tmpFirtst_name,_tmpLast_name,_tmpGender,_tmpHire_date);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public Empleado buscarPorNombre(final String n) {
    final String _sql = "SELECT * FROM Empleado WHERE first_name LIKE ?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    if (n == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, n);
    }
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfEmpNo = CursorUtil.getColumnIndexOrThrow(_cursor, "emp_no");
      final int _cursorIndexOfBirthDate = CursorUtil.getColumnIndexOrThrow(_cursor, "birth_date");
      final int _cursorIndexOfFirtstName = CursorUtil.getColumnIndexOrThrow(_cursor, "first_name");
      final int _cursorIndexOfLastName = CursorUtil.getColumnIndexOrThrow(_cursor, "last_name");
      final int _cursorIndexOfGender = CursorUtil.getColumnIndexOrThrow(_cursor, "gender");
      final int _cursorIndexOfHireDate = CursorUtil.getColumnIndexOrThrow(_cursor, "hire_date");
      final Empleado _result;
      if(_cursor.moveToFirst()) {
        final int _tmpEmp_no;
        _tmpEmp_no = _cursor.getInt(_cursorIndexOfEmpNo);
        final String _tmpBirth_date;
        if (_cursor.isNull(_cursorIndexOfBirthDate)) {
          _tmpBirth_date = null;
        } else {
          _tmpBirth_date = _cursor.getString(_cursorIndexOfBirthDate);
        }
        final String _tmpFirtst_name;
        if (_cursor.isNull(_cursorIndexOfFirtstName)) {
          _tmpFirtst_name = null;
        } else {
          _tmpFirtst_name = _cursor.getString(_cursorIndexOfFirtstName);
        }
        final String _tmpLast_name;
        if (_cursor.isNull(_cursorIndexOfLastName)) {
          _tmpLast_name = null;
        } else {
          _tmpLast_name = _cursor.getString(_cursorIndexOfLastName);
        }
        final String _tmpGender;
        if (_cursor.isNull(_cursorIndexOfGender)) {
          _tmpGender = null;
        } else {
          _tmpGender = _cursor.getString(_cursorIndexOfGender);
        }
        final String _tmpHire_date;
        if (_cursor.isNull(_cursorIndexOfHireDate)) {
          _tmpHire_date = null;
        } else {
          _tmpHire_date = _cursor.getString(_cursorIndexOfHireDate);
        }
        _result = new Empleado(_tmpEmp_no,_tmpBirth_date,_tmpFirtst_name,_tmpLast_name,_tmpGender,_tmpHire_date);
      } else {
        _result = null;
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public List<Empleado> optenerFiltrando(final String c) {
    final String _sql = "SELECT * FROM Empleado WHERE emp_no LIKE ?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    if (c == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, c);
    }
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfEmpNo = CursorUtil.getColumnIndexOrThrow(_cursor, "emp_no");
      final int _cursorIndexOfBirthDate = CursorUtil.getColumnIndexOrThrow(_cursor, "birth_date");
      final int _cursorIndexOfFirtstName = CursorUtil.getColumnIndexOrThrow(_cursor, "first_name");
      final int _cursorIndexOfLastName = CursorUtil.getColumnIndexOrThrow(_cursor, "last_name");
      final int _cursorIndexOfGender = CursorUtil.getColumnIndexOrThrow(_cursor, "gender");
      final int _cursorIndexOfHireDate = CursorUtil.getColumnIndexOrThrow(_cursor, "hire_date");
      final List<Empleado> _result = new ArrayList<Empleado>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final Empleado _item;
        final int _tmpEmp_no;
        _tmpEmp_no = _cursor.getInt(_cursorIndexOfEmpNo);
        final String _tmpBirth_date;
        if (_cursor.isNull(_cursorIndexOfBirthDate)) {
          _tmpBirth_date = null;
        } else {
          _tmpBirth_date = _cursor.getString(_cursorIndexOfBirthDate);
        }
        final String _tmpFirtst_name;
        if (_cursor.isNull(_cursorIndexOfFirtstName)) {
          _tmpFirtst_name = null;
        } else {
          _tmpFirtst_name = _cursor.getString(_cursorIndexOfFirtstName);
        }
        final String _tmpLast_name;
        if (_cursor.isNull(_cursorIndexOfLastName)) {
          _tmpLast_name = null;
        } else {
          _tmpLast_name = _cursor.getString(_cursorIndexOfLastName);
        }
        final String _tmpGender;
        if (_cursor.isNull(_cursorIndexOfGender)) {
          _tmpGender = null;
        } else {
          _tmpGender = _cursor.getString(_cursorIndexOfGender);
        }
        final String _tmpHire_date;
        if (_cursor.isNull(_cursorIndexOfHireDate)) {
          _tmpHire_date = null;
        } else {
          _tmpHire_date = _cursor.getString(_cursorIndexOfHireDate);
        }
        _item = new Empleado(_tmpEmp_no,_tmpBirth_date,_tmpFirtst_name,_tmpLast_name,_tmpGender,_tmpHire_date);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public Empleado optenerUno(final String c) {
    final String _sql = "SELECT * FROM Empleado WHERE emp_no =?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    if (c == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, c);
    }
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfEmpNo = CursorUtil.getColumnIndexOrThrow(_cursor, "emp_no");
      final int _cursorIndexOfBirthDate = CursorUtil.getColumnIndexOrThrow(_cursor, "birth_date");
      final int _cursorIndexOfFirtstName = CursorUtil.getColumnIndexOrThrow(_cursor, "first_name");
      final int _cursorIndexOfLastName = CursorUtil.getColumnIndexOrThrow(_cursor, "last_name");
      final int _cursorIndexOfGender = CursorUtil.getColumnIndexOrThrow(_cursor, "gender");
      final int _cursorIndexOfHireDate = CursorUtil.getColumnIndexOrThrow(_cursor, "hire_date");
      final Empleado _result;
      if(_cursor.moveToFirst()) {
        final int _tmpEmp_no;
        _tmpEmp_no = _cursor.getInt(_cursorIndexOfEmpNo);
        final String _tmpBirth_date;
        if (_cursor.isNull(_cursorIndexOfBirthDate)) {
          _tmpBirth_date = null;
        } else {
          _tmpBirth_date = _cursor.getString(_cursorIndexOfBirthDate);
        }
        final String _tmpFirtst_name;
        if (_cursor.isNull(_cursorIndexOfFirtstName)) {
          _tmpFirtst_name = null;
        } else {
          _tmpFirtst_name = _cursor.getString(_cursorIndexOfFirtstName);
        }
        final String _tmpLast_name;
        if (_cursor.isNull(_cursorIndexOfLastName)) {
          _tmpLast_name = null;
        } else {
          _tmpLast_name = _cursor.getString(_cursorIndexOfLastName);
        }
        final String _tmpGender;
        if (_cursor.isNull(_cursorIndexOfGender)) {
          _tmpGender = null;
        } else {
          _tmpGender = _cursor.getString(_cursorIndexOfGender);
        }
        final String _tmpHire_date;
        if (_cursor.isNull(_cursorIndexOfHireDate)) {
          _tmpHire_date = null;
        } else {
          _tmpHire_date = _cursor.getString(_cursorIndexOfHireDate);
        }
        _result = new Empleado(_tmpEmp_no,_tmpBirth_date,_tmpFirtst_name,_tmpLast_name,_tmpGender,_tmpHire_date);
      } else {
        _result = null;
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  public static List<Class<?>> getRequiredConverters() {
    return Collections.emptyList();
  }
}
