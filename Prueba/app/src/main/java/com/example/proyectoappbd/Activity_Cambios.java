package com.example.proyectoappbd;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import BaseDatos.Employees;
import Entidad.Empleado;

public class Activity_Cambios extends Activity{
    EditText emp_no;
    EditText birth_date,first_name,last_name,hire_date;
    Spinner gender;
    RecyclerView recicler;
    RecyclerView.Adapter adaper;
    RecyclerView.LayoutManager layoutManager;
    List<Empleado> e;

    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_altas);
        emp_no=findViewById(R.id.txt_alEmp_no);
        birth_date=findViewById(R.id.txt_alBirthDate);
        first_name=findViewById(R.id.txt_alFirstName);
        last_name=findViewById(R.id.txt_alLastName);
        hire_date=findViewById(R.id.txt_alHireDate);
        gender=findViewById(R.id.spn_alGender);

        String[] datos = {""};
        recicler=findViewById(R.id.rv_cb);

        recicler.setHasFixedSize(true);

        layoutManager=new LinearLayoutManager(this);
        recicler.setLayoutManager(layoutManager);
        int[] c = new int[1];

        emp_no.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(!emp_no.getText().toString().isEmpty()){
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            Employees conexion=Employees.gettAppDatabase(getBaseContext());
                            e=null;
                            e=conexion.empleadoDAO().optenerFiltrando(emp_no.getText().toString()+"%");
                            c[0] =e.size();
                            datos[0]="";
                            for(int i=0;i<c[0];i++){
                                System.out.println(i+"--> "+e.get(i).toString());
                                datos[0] = datos[0]+e.get(i).toString()+"/";
                            }
                            System.out.println("Datos----->"+datos[0]);
                            adaper=new Adaptador(datos[0].split("/"));
                            recicler.setAdapter(adaper);
                        }
                    }).start();

                }else{
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            Employees conexion=Employees.gettAppDatabase(getBaseContext());
                            e= conexion.empleadoDAO().optenerTodos();
                            c[0] =e.size();
                            datos[0]="";
                            for(int i=0;i<c[0];i++){
                                datos[0] = datos[0]+e.get(i).toString()+"/";
                            }
                            System.out.println("Datos----->"+datos[0]);
                            adaper=new Adaptador(datos[0].split("/"));
                            recicler.setAdapter(adaper);
                        }
                    }).start();
                }
            }
        });
    }
    public void buscar_cambios(View v){
        if(emp_no.getText().toString().isEmpty()==false){
            new Thread(new Runnable() {
                @Override
                public void run() {
                    Empleado alu=null;
                    Employees conexionBD=Employees.gettAppDatabase(getBaseContext());
                    alu = conexionBD.empleadoDAO().optenerUno(emp_no.getText().toString());
                    if(alu!=null){
                        emp_no.setText(alu.getFirtst_name().toString());
                    }else{

                    }
                }
            }).start();
        }
    }
    public void cambiar_empleado(View v){
        if(emp_no.getText().toString().isEmpty()==false){
            new Thread(new Runnable() {
                @Override
                public void run() {
                    Employees conexionBD=Employees.gettAppDatabase(getBaseContext());
                    e=conexionBD.empleadoDAO().optenerFiltrando(emp_no.getText().toString());
                    if(e!=null){
                        String cadenaEmp_no = emp_no.getText().toString();
                        int empNo = Integer.parseInt(cadenaEmp_no);
                        cadenaEmp_no="";
                        String gen = (String) gender.getSelectedItem();
                        String genero;
                        if(gen.equals("Masculino")){
                            genero = "M";
                        }else{
                            genero = "F";
                        }
                        conexionBD.empleadoDAO().actualizarProNoControl(empNo,birth_date.getText().toString(),first_name.getText().toString(),last_name.getText().toString(),genero,hire_date.getText().toString());
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getBaseContext(),"El registro se actualizo ",Toast.LENGTH_LONG).show();

                            }
                        });
                    }
                }
            }).start();
        }
    }
}
