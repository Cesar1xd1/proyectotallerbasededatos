/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package vista;

import conexionBD.Conexion;
import controlador.EmpleadoDAO;
import java.awt.Color;
import java.sql.Connection;
import java.util.HashMap;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;
import javax.servlet.ServletOutputStream;
/**
 *
 * @author cesar
 */
public class VentanaPrincipal extends javax.swing.JFrame {
    
    /**
     * Creates new form VentanaPrincipal
     */
    public VentanaPrincipal() {
        initComponents();
        getContentPane().setBackground(new Color(204,204,255));
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenu1 = new javax.swing.JMenu();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenuAltas = new javax.swing.JMenu();
        jmAltaEmpleado = new javax.swing.JMenuItem();
        jmAltaDepartamento = new javax.swing.JMenuItem();
        jmAltaAdministrador = new javax.swing.JMenuItem();
        JMenuBajas = new javax.swing.JMenu();
        jmBajaEmpleado = new javax.swing.JMenuItem();
        jmBajaDepartamento = new javax.swing.JMenuItem();
        jmBajaAdministrador = new javax.swing.JMenuItem();
        jMenuCambios = new javax.swing.JMenu();
        jmCambioEmpleado = new javax.swing.JMenuItem();
        jmCambioDepartamento = new javax.swing.JMenuItem();
        jmCambioAdministrador = new javax.swing.JMenuItem();
        jMenuConsultas = new javax.swing.JMenu();
        jmConsultaEmpleado = new javax.swing.JMenuItem();
        jmConsultaDepartamento = new javax.swing.JMenuItem();
        jmConsultaAdministrador = new javax.swing.JMenuItem();
        jMenuReporte = new javax.swing.JMenu();
        jmReporteEmpleado = new javax.swing.JMenuItem();
        jMenuVista = new javax.swing.JMenu();
        jmVistaAdministrador = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();

        jMenu1.setText("jMenu1");

        setBackground(new java.awt.Color(204, 204, 255));
        setResizable(false);

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconos/bienvenido-de-nuevo.png"))); // NOI18N

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconos/recursos-humanos.png"))); // NOI18N

        jLabel3.setFont(new java.awt.Font("Yu Gothic Light", 1, 36)); // NOI18N
        jLabel3.setText("Employees Corp.");

        jMenuAltas.setText("Altas");
        jMenuAltas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuAltasActionPerformed(evt);
            }
        });

        jmAltaEmpleado.setText("Añadir Empleado");
        jmAltaEmpleado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmAltaEmpleadoActionPerformed(evt);
            }
        });
        jMenuAltas.add(jmAltaEmpleado);

        jmAltaDepartamento.setText("Añadir Departamento");
        jmAltaDepartamento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmAltaDepartamentoActionPerformed(evt);
            }
        });
        jMenuAltas.add(jmAltaDepartamento);

        jmAltaAdministrador.setText("Añadir Administrador");
        jmAltaAdministrador.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmAltaAdministradorActionPerformed(evt);
            }
        });
        jMenuAltas.add(jmAltaAdministrador);

        jMenuBar1.add(jMenuAltas);

        JMenuBajas.setText("Bajas");

        jmBajaEmpleado.setText("Eliminar Empleado");
        jmBajaEmpleado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmBajaEmpleadoActionPerformed(evt);
            }
        });
        JMenuBajas.add(jmBajaEmpleado);

        jmBajaDepartamento.setText("Eliminar Departamento");
        jmBajaDepartamento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmBajaDepartamentoActionPerformed(evt);
            }
        });
        JMenuBajas.add(jmBajaDepartamento);

        jmBajaAdministrador.setText("Eliminar Administrador");
        jmBajaAdministrador.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmBajaAdministradorActionPerformed(evt);
            }
        });
        JMenuBajas.add(jmBajaAdministrador);

        jMenuBar1.add(JMenuBajas);

        jMenuCambios.setText("Cambios");

        jmCambioEmpleado.setText("Modificar Empleado");
        jmCambioEmpleado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmCambioEmpleadoActionPerformed(evt);
            }
        });
        jMenuCambios.add(jmCambioEmpleado);

        jmCambioDepartamento.setText("Modificar Departamento");
        jmCambioDepartamento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmCambioDepartamentoActionPerformed(evt);
            }
        });
        jMenuCambios.add(jmCambioDepartamento);

        jmCambioAdministrador.setText("Modificar Administrador");
        jmCambioAdministrador.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmCambioAdministradorActionPerformed(evt);
            }
        });
        jMenuCambios.add(jmCambioAdministrador);

        jMenuBar1.add(jMenuCambios);

        jMenuConsultas.setText("Consultas");

        jmConsultaEmpleado.setText("Buscar Empleado");
        jmConsultaEmpleado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmConsultaEmpleadoActionPerformed(evt);
            }
        });
        jMenuConsultas.add(jmConsultaEmpleado);

        jmConsultaDepartamento.setText("Buscar Departamento");
        jmConsultaDepartamento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmConsultaDepartamentoActionPerformed(evt);
            }
        });
        jMenuConsultas.add(jmConsultaDepartamento);

        jmConsultaAdministrador.setText("Buscar Administrador");
        jmConsultaAdministrador.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmConsultaAdministradorActionPerformed(evt);
            }
        });
        jMenuConsultas.add(jmConsultaAdministrador);

        jMenuBar1.add(jMenuConsultas);

        jMenuReporte.setText("Reporte");

        jmReporteEmpleado.setText("Reporte Empleado");
        jmReporteEmpleado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmReporteEmpleadoActionPerformed(evt);
            }
        });
        jMenuReporte.add(jmReporteEmpleado);

        jMenuBar1.add(jMenuReporte);

        jMenuVista.setText("Vista");

        jmVistaAdministrador.setText("Vista Administrador");
        jmVistaAdministrador.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmVistaAdministradorActionPerformed(evt);
            }
        });
        jMenuVista.add(jmVistaAdministrador);

        jMenuBar1.add(jMenuVista);

        jMenu2.setText("Respaldo");

        jMenuItem1.setText("Respaldo Departamento");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem1);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(14, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(41, 41, 41))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel1)
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(39, 39, 39)
                        .addComponent(jLabel3)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2)
                .addGap(0, 81, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jmCambioDepartamentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmCambioDepartamentoActionPerformed
        new VentanaCambioDepartamento().setVisible(true);
    }//GEN-LAST:event_jmCambioDepartamentoActionPerformed

    private void jmConsultaDepartamentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmConsultaDepartamentoActionPerformed
        new VentanaConsultaDepartamento().setVisible(true);
    }//GEN-LAST:event_jmConsultaDepartamentoActionPerformed

    private void jmConsultaAdministradorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmConsultaAdministradorActionPerformed
       new VentanaConsultaAdministrador().setVisible(true);
    }//GEN-LAST:event_jmConsultaAdministradorActionPerformed

    private void jMenuAltasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuAltasActionPerformed
        
    }//GEN-LAST:event_jMenuAltasActionPerformed

    private void jmAltaEmpleadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmAltaEmpleadoActionPerformed
        new VentanaAltaEmpleado().setVisible(true);
       
    }//GEN-LAST:event_jmAltaEmpleadoActionPerformed

    private void jmAltaDepartamentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmAltaDepartamentoActionPerformed
        new VentanaAltaDepartamento().setVisible(true);
    }//GEN-LAST:event_jmAltaDepartamentoActionPerformed

    private void jmAltaAdministradorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmAltaAdministradorActionPerformed
        new VentanaAltaAdministrador().setVisible(true);
    }//GEN-LAST:event_jmAltaAdministradorActionPerformed

    private void jmBajaEmpleadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmBajaEmpleadoActionPerformed
        new VentanaBajaEmpleado().setVisible(true);
    }//GEN-LAST:event_jmBajaEmpleadoActionPerformed

    private void jmBajaDepartamentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmBajaDepartamentoActionPerformed
        new VentanaBajaDepartamento().setVisible(true);
    }//GEN-LAST:event_jmBajaDepartamentoActionPerformed

    private void jmBajaAdministradorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmBajaAdministradorActionPerformed
        new VentanaBajaAdminstrador().setVisible(true);
    }//GEN-LAST:event_jmBajaAdministradorActionPerformed

    private void jmCambioEmpleadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmCambioEmpleadoActionPerformed
        new VentanaCambioEmpleado().setVisible(true);
    }//GEN-LAST:event_jmCambioEmpleadoActionPerformed

    private void jmCambioAdministradorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmCambioAdministradorActionPerformed
        new VentanaCambioAdministrador().setVisible(true);
    }//GEN-LAST:event_jmCambioAdministradorActionPerformed

    private void jmConsultaEmpleadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmConsultaEmpleadoActionPerformed
new VentanaConsultaEmpleado().setVisible(true);        // TODO add your handling code here:
    }//GEN-LAST:event_jmConsultaEmpleadoActionPerformed

    private void jmReporteEmpleadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmReporteEmpleadoActionPerformed
         Conexion con = new Conexion();
        try {
            String ruta=System.getProperty("user.dir")+"/src/r/report1.jasper";
            JasperReport jaspe=(JasperReport) JRLoader.loadObjectFromFile(ruta);
            JasperPrint print=JasperFillManager.fillReport(jaspe,null,con.getConexion());
            JasperViewer view= new JasperViewer(print,false);
            view.setVisible(true);
        } catch (Exception e) {
            System.err.println("Error al generar el reporte---->"+e.getMessage());
        }
    }//GEN-LAST:event_jmReporteEmpleadoActionPerformed

    private void jmVistaAdministradorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmVistaAdministradorActionPerformed
        new VentanaVistaAdministrador().setVisible(true);
        
    }//GEN-LAST:event_jmVistaAdministradorActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        new VentanaRespaldoDepartamento().setVisible(true);
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VentanaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VentanaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VentanaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VentanaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new VentanaPrincipal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenu JMenuBajas;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenuAltas;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenu jMenuCambios;
    private javax.swing.JMenu jMenuConsultas;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenu jMenuReporte;
    private javax.swing.JMenu jMenuVista;
    private javax.swing.JMenuItem jmAltaAdministrador;
    private javax.swing.JMenuItem jmAltaDepartamento;
    private javax.swing.JMenuItem jmAltaEmpleado;
    private javax.swing.JMenuItem jmBajaAdministrador;
    private javax.swing.JMenuItem jmBajaDepartamento;
    private javax.swing.JMenuItem jmBajaEmpleado;
    private javax.swing.JMenuItem jmCambioAdministrador;
    private javax.swing.JMenuItem jmCambioDepartamento;
    private javax.swing.JMenuItem jmCambioEmpleado;
    private javax.swing.JMenuItem jmConsultaAdministrador;
    private javax.swing.JMenuItem jmConsultaDepartamento;
    private javax.swing.JMenuItem jmConsultaEmpleado;
    private javax.swing.JMenuItem jmReporteEmpleado;
    private javax.swing.JMenuItem jmVistaAdministrador;
    // End of variables declaration//GEN-END:variables
}
