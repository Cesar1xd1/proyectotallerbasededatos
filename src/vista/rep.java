/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vista;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;
import conexionBD.Conexion;
import java.sql.Connection;
import java.util.HashMap;


/**
 *
 * @author cesar
 */
public class rep {
 
    
    public void mostrarReporte(){
            Conexion con = new Conexion();
            Connection conexion = con.getConexion();
        try {
            
            String ruta=System.getProperty("user.dir")+"/src/r/report1.jasper";
            JasperReport jaspe=(JasperReport) JRLoader.loadObjectFromFile(ruta);
            JasperPrint print=JasperFillManager.fillReport(jaspe,null,conexion);
            JasperViewer view= new JasperViewer(print,false);
            view.setVisible(true);
        } catch (Exception e) {
            System.err.println("Error al generar el reporte---->"+e.getMessage());
        }

    }

    public rep() {
    }
}
