
package controlador;
import com.sun.glass.ui.Cursor;
import conexionBD.Conexion;
import modelo.Empleado;

public class EmpleadoDAO {
    Conexion conexion;

    public EmpleadoDAO() {
        conexion = new Conexion();
    }
    public void select(){
        conexion.ejecutarInstruccion("SELECT * FROM employees");
    }
    
    public boolean insertarEmpleado(Empleado em){
        boolean resultado = false;
        String sql = "INSERT INTO employees VALUES('"+em.getNoEmpleado()+"','"+em.getFechaNacimiento()+"','"+em.getNombre()+"','"+em.getApellido()+"','"+em.getGenero()+"','"+em.getFechaContrato()+"');";
        resultado = conexion.ejecutarInstruccion(sql);
        return resultado;
    }
    
    public boolean eliminarEmpleado(String ne) {
		boolean resultado = false;
		String sql = "DELETE FROM employees WHERE emp_no = \""+ne+"\"";
		resultado = conexion.ejecutarInstruccion(sql);
		
		return resultado;
	}
    public boolean modificarEmpleado(Empleado em) {
		boolean resultado = false;
		String sql = "UPDATE employees SET birth_date='"+em.getFechaNacimiento()+"', first_name='"+em.getNombre()+"', last_name='"+em.getApellido()+"'"+", gender='"+em.getGenero()+"', hire_date='"+em.getFechaContrato()+"' "+ " WHERE emp_no = '"+em.getNoEmpleado()+"';";
                resultado = conexion.ejecutarInstruccion(sql);
		
		return resultado;
	}

}
