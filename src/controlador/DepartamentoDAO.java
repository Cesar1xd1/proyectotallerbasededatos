
package controlador;

import conexionBD.Conexion;
import modelo.Departamento;


public class DepartamentoDAO {
    Conexion conexion;

    public DepartamentoDAO() {
        conexion = new Conexion();
    }
    public void it(){
        String sql = "START TRANSACTION";
        conexion.ejecutarInstruccion(sql);
    }
    public void rb(){
        String sql = "ROLLBAKC";
        conexion.ejecutarInstruccion(sql);
    }
    public void cm(){
        String sql = "COMMIT";
        conexion.ejecutarInstruccion(sql);
    }
    
    
    public boolean insertarDepartamento(Departamento d) {
		boolean resultado = false;
		String sql = "INSERT INTO departments VALUES('"+d.getNoDepartamento()+"','"+d.getNombreDepartamento()+"');";
		resultado = conexion.ejecutarInstruccion(sql);
		
		return resultado;
	}
    public boolean eliminarDepartamento(String nd) {
		boolean resultado = false;
		String sql = "DELETE FROM departments WHERE dept_no = \""+nd+"\"";
		resultado = conexion.ejecutarInstruccion(sql);
		
		return resultado;
	}
    	public boolean modificarDepartamento(Departamento d) {
		boolean resultado = false;
		
		String sql = "UPDATE departments SET dept_name='"+d.getNombreDepartamento()+"' "
                + " WHERE dept_no = '"+d.getNoDepartamento()+"';";
		resultado = conexion.ejecutarInstruccion(sql);
		
		return resultado;
	}
}
