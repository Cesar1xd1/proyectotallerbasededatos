/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package conexionBD;

import java.sql.*;

public class Conexion {
	private static Connection conexion=null;
	private static Statement stn;
	private static ResultSet rs;
	
        public static Connection getConexion(){

         if (conexion == null){
             new Conexion();
         }

         return conexion;
   }
        
	public Conexion() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		
			String URL = "jdbc:mysql://localhost:3306/employees";
			
			
			conexion = DriverManager.getConnection(URL,"Cesar1xd1","CSRxd123");
		} catch (ClassNotFoundException e) {
			System.out.println("Error de DRIVER");
		} catch (SQLException throwables) {
			System.out.println("Error en la conexion de MySQL");
		}
	}
        
          
	public void cerrarConexion() {
		try {
			stn.close();
		} catch (SQLException e) {
			System.out.println("Error al cerrar la conexion");
			e.printStackTrace();
		}
	}
	
	//__ METODO PARA OPERACIONES DDL Y DML (ABC - ALTAS,BASJAS,CAMBIOS)
	public boolean ejecutarInstruccion(String sql) {
		try {
			stn = conexion.createStatement();
			int resultado = stn.executeUpdate(sql);
			return resultado==1?true:false;
		} catch (SQLException e) {
			return false;
		}
		
	}
		
	
	//__METODO PARA OPERACIONES DE CONSULTA
	public ResultSet ejecutarConsulta(String sql) {
		try {
			stn = conexion.createStatement();
			rs = stn.executeQuery(sql);
		} catch (SQLException e) {
			System.out.println("Se pudo ejecutar la consulta");
			e.printStackTrace();
		}
		return rs;
	}
	
}